package com.netcracker.edu.collection;

import com.netcracker.edu.collection.entity.GeometricFigure;
import com.netcracker.edu.collection.service.list.ArrayListCollectionOperation;
import com.netcracker.edu.collection.service.list.DeclarativeArrayListCollectionOperation;
import com.netcracker.edu.collection.service.map.DeclarativeHashMapCollectionOperation;
import com.netcracker.edu.collection.service.map.HashMapCollectionOperation;

public class Main {

    public static void main(String[] args) {

        System.out.println("===================================================================\n"
                + "ImperativeArrayList");
        checkImperativeArrayList();
        System.out.println("===================================================================\n"
                + "ImperativeHashMap");
        checkImperativeHashMap();
        System.out.println("===================================================================\n"
                + "DeclarativeArrayList");
        checkDeclarativeArrayList();
        System.out.println("===================================================================\n"
                + "DeclarativeHashMap");
        checkDeclarativeHashMap();

    }

    public static void checkImperativeArrayList() {
        GeometricFigure geometricFigure1 = new GeometricFigure("Квадрат", 4);
        GeometricFigure geometricFigure2 = new GeometricFigure("Треугольник", 3);
        GeometricFigure geometricFigure3 = new GeometricFigure("Пятиугольник", 5);
        GeometricFigure geometricFigure4 = new GeometricFigure("Прямоугольник", 4);
        ArrayListCollectionOperation geometricList = new ArrayListCollectionOperation();
        geometricList.add(geometricFigure1);
        geometricList.add(geometricFigure2);
        geometricList.add(geometricFigure3);
        geometricList.add(geometricFigure4);

        System.out.println(geometricList.toString());
        System.out.print("getSumColDot() = ");
        System.out.println(geometricList.getSumColDot());
        System.out.print("get(2) = ");
        System.out.println(geometricList.get(2).toString());
        System.out.print("getTwoItem() = ");
        System.out.println(geometricList.getTwoItem(1).toString());
        System.out.print("getItems() = ");
        System.out.println(geometricList.getItems(geometricFigure1).toString());
        System.out.print("isAllItemsNameNotNull() = ");
        System.out.println(geometricList.isAllItemsNameNotNull());
        System.out.print("addPostfix(_1) = ");
        geometricList.addPostfix("_1");
        System.out.println(geometricList.toString());
        System.out.print("sortByName() = ");
        geometricList.sortByName();
        System.out.println(geometricList.toString());
        System.out.print("sortByColDot() = ");
        geometricList.sortByColDot();
        System.out.println(geometricList.toString());
        System.out.print("removeByMinName() ");
        geometricList.removeByMinName();
        System.out.println(geometricList.toString());
        System.out.print("removeByMaxColDot()  ");
        geometricList.removeByMaxColDot();
        System.out.println(geometricList.toString());
        System.out.print("removeItemsWithColDotlLessThem(4) ");
        geometricList.removeItemsWithColDotLessThan(4);
        System.out.println(geometricList.toString());

    }

    public static void checkImperativeHashMap() {
        GeometricFigure geometricFigure1 = new GeometricFigure("Квадрат", 4);
        GeometricFigure geometricFigure2 = new GeometricFigure("Треугольник", 3);
        GeometricFigure geometricFigure3 = new GeometricFigure("Пятиугольник", 5);
        GeometricFigure geometricFigure4 = new GeometricFigure("Прямоугольник", 4);
        HashMapCollectionOperation geometricList = new HashMapCollectionOperation();
        geometricList.put("0", geometricFigure1);
        geometricList.put("1", geometricFigure2);
        geometricList.put("2", geometricFigure3);
        geometricList.put("3", geometricFigure4);

        geometricList.printItem();
        System.out.print("getSumColDot() = ");
        System.out.println(geometricList.getSumColDot());
        System.out.print("get(2) = ");
        System.out.println(geometricList.get(2).toString());
        System.out.print("getTwoItem() = ");
        System.out.println(geometricList.getTwoItem(1).toString());
        System.out.print("getItems() = ");
        System.out.println(geometricList.getItems(geometricFigure1).toString());
        System.out.print("isAllItemsNameNotNull() = ");
        System.out.println(geometricList.isAllItemsNameNotNull());
        System.out.print("addPostfix(_1) = ");
        geometricList.addPostfix("_1");
        geometricList.printItem();
        System.out.print("sortByName() = ");
        geometricList.sortByName();
        geometricList.printItem();
        System.out.print("sortByColDot() = ");
        geometricList.sortByColDot();
        geometricList.printItem();
        System.out.print("removeByMinName() ");
        geometricList.removeByMinName();
        geometricList.printItem();
        System.out.print("removeByMaxColDot()  ");
        geometricList.removeByMaxColDot();
        geometricList.printItem();
        System.out.print("removeItemsWithColDotlLessThem(4) ");
        geometricList.removeItemsWithColDotLessThan(4);
        geometricList.printItem();

    }

    public static void checkDeclarativeArrayList() {
        GeometricFigure geometricFigure1 = new GeometricFigure("Квадрат", 4);
        GeometricFigure geometricFigure2 = new GeometricFigure("Треугольник", 3);
        GeometricFigure geometricFigure3 = new GeometricFigure("Пятиугольник", 5);
        GeometricFigure geometricFigure4 = new GeometricFigure("Прямоугольник", 4);
        DeclarativeArrayListCollectionOperation geometricList = new DeclarativeArrayListCollectionOperation();
        geometricList.add(geometricFigure1);
        geometricList.add(geometricFigure2);
        geometricList.add(geometricFigure3);
        geometricList.add(geometricFigure4);

        geometricList.printElem();
        System.out.print("getSumColDot() = ");
        System.out.println(geometricList.getSumColDot());
        System.out.print("get(2) = ");
        System.out.println(geometricList.get(2).toString());
        System.out.print("getTwoItem() = ");
        System.out.println(geometricList.getTwoItem(1).toString());
        System.out.print("getItems() = ");
        System.out.println(geometricList.getItems(geometricFigure1).toString());
        System.out.print("isAllItemsNameNotNull() = ");
        System.out.println(geometricList.isAllItemsNameNotNull());
        System.out.print("addPostfix(_1) = ");
        geometricList.addPostfix("_1");
        geometricList.printElem();
        System.out.print("sortByName() = ");
        geometricList.sortByName();
        geometricList.printElem();
        System.out.print("sortByColDot() = ");
        geometricList.sortByColDot();
        geometricList.printElem();
        System.out.print("removeByMinName() ");
        geometricList.removeByMinName();
        geometricList.printElem();
        System.out.print("removeByMaxColDot()  ");
        geometricList.removeByMaxColDot();
        geometricList.printElem();
        System.out.print("removeItemsWithColDotlLessThem(4) ");
        geometricList.removeItemsWithColDotLessThan(4);
        geometricList.printElem();

    }

    public static void checkDeclarativeHashMap() {
        GeometricFigure geometricFigure1 = new GeometricFigure("Квадрат", 4);
        GeometricFigure geometricFigure2 = new GeometricFigure("Треугольник", 3);
        GeometricFigure geometricFigure3 = new GeometricFigure("Пятиугольник", 5);
        GeometricFigure geometricFigure4 = new GeometricFigure("Прямоугольник", 4);
        DeclarativeHashMapCollectionOperation geometricList = new DeclarativeHashMapCollectionOperation();
        geometricList.put("0", geometricFigure1);
        geometricList.put("1", geometricFigure2);
        geometricList.put("2", geometricFigure3);
        geometricList.put("3", geometricFigure4);

        geometricList.printElem();
        System.out.print("getSumColDot() = ");
        System.out.println(geometricList.getSumColDot());
        System.out.print("get(2) = ");
        System.out.println(geometricList.get(2).toString());
        System.out.print("getTwoItem() = ");
        System.out.println(geometricList.getTwoItem(1).toString());
        System.out.print("getItems() = ");
        System.out.println(geometricList.getItems(geometricFigure1).toString());
        System.out.print("isAllItemsNameNotNull() = ");
        System.out.println(geometricList.isAllItemsNameNotNull());
        System.out.print("addPostfix(_1) = ");
        geometricList.addPostfix("_1");
        geometricList.printElem();
        System.out.print("sortByName() = ");
        geometricList.sortByName();
        geometricList.printElem();
        System.out.print("sortByColDot() = ");
        geometricList.sortByColDot();
        geometricList.printElem();
        System.out.print("removeByMinName() ");
        geometricList.removeByMinName();
        geometricList.printElem();
        System.out.print("removeByMaxColDot()  ");
        geometricList.removeByMaxColDot();
        geometricList.printElem();
        System.out.print("removeItemsWithColDotlLessThem(4) ");
        geometricList.removeItemsWithColDotLessThan(4);
        geometricList.printElem();

    }

}
