package com.netcracker.edu.collection.service.list;

import com.netcracker.edu.collection.entity.GeometricFigure;
import com.netcracker.edu.collection.service.CollectionOperation;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class DeclarativeArrayListCollectionOperation implements CollectionOperation {

    private List<GeometricFigure> list = new ArrayList<>();

    public void add(GeometricFigure geometricFigure) {
        this.list.add(geometricFigure);
    }

    @Override
    public void removeByMinName() {
        String minName =
                list.stream()
                        .min(Comparator.comparing(GeometricFigure::getName))
                        .get().getName();

        this.list = list.stream()
                .filter(geometricFigure -> minName != geometricFigure.getName())
                .collect(Collectors.toCollection(ArrayList::new));

    }

    @Override
    public void removeByMaxColDot() {
        int maxColDot =
                list.stream()
                        .min(Comparator.comparing(GeometricFigure::getColDot))
                        .get().getColDot();

        this.list = list.stream()
                .filter(geometricFigure -> maxColDot != geometricFigure.getColDot())
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @Override
    public void removeItemsWithColDotLessThan(int value) {
        this.list = list.stream()
                .filter(geometricFigure -> value >= geometricFigure.getColDot())
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @Override
    public int getSumColDot() {
        return list.stream()
                .mapToInt(GeometricFigure::getColDot)
                .sum();
    }

    @Override
    public GeometricFigure get(int index) {
        return list.stream()
                .skip(index - 1)
                .findFirst()
                .get();
    }

    @Override
    public List<GeometricFigure> getTwoItem(int index) {

        ArrayList<GeometricFigure> resultList =
                list.stream()
                        .skip(index - 1)
                        .limit(2)
                        .collect(Collectors.toCollection(ArrayList::new));
        return resultList;
    }

    @Override
    public List<GeometricFigure> getItems(GeometricFigure geometricFigure) {
        ArrayList<GeometricFigure> resultList =
                list.stream()
                        .filter(Figure -> Figure.equals(geometricFigure))
                        .collect(Collectors.toCollection(ArrayList::new));
        return resultList;
    }

    @Override
    public boolean isAllItemsNameNotNull() {
        return list.stream()
                .filter(geometricFigure -> null != geometricFigure.getName())
                .count() == list.stream()
                .count();
    }

    @Override
    public void addPostfix(String postfix) {
        list.stream()
                .forEach(geometricFigure -> geometricFigure.setName(geometricFigure.getName().concat(postfix)));
    }

    public void sortByName() {

        this.list = list.stream()
                .sorted(Comparator.comparing(GeometricFigure::getName))
                .collect(Collectors.toCollection(ArrayList::new));

    }

    public void sortByColDot() {
        this.list = list.stream()
                .sorted(Comparator.comparing(GeometricFigure::getColDot))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public void printElem() {
        list.stream()
                .forEach(geometricFigure -> System.out.println(geometricFigure));
    }
}
