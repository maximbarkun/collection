package com.netcracker.edu.collection.service.map;

import com.netcracker.edu.collection.entity.GeometricFigure;
import com.netcracker.edu.collection.service.CollectionOperation;

import java.util.*;
import java.util.stream.Collectors;

public class DeclarativeHashMapCollectionOperation implements CollectionOperation {

    private HashMap<String, GeometricFigure> map = new HashMap<>();

    public void put(String key, GeometricFigure geometricFigure) {
        this.map.put(key, geometricFigure);
    }

    @Override
    public void removeByMinName() {
        String minName = map.values()
                .stream()
                .min(Comparator.comparing(GeometricFigure::getName))
                .get()
                .getName();

        map = map.entrySet()
                .stream()
                .filter(p -> p.getValue().getName() != minName)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (o1, o2) -> o1, HashMap::new));
    }

    @Override
    public void removeByMaxColDot() {
        int maxColDot = map.values()
                .stream()
                .min(Comparator.comparing(GeometricFigure::getColDot))
                .get()
                .getColDot();

        map = map.entrySet()
                .stream()
                .filter(p -> p.getValue().getColDot() != maxColDot)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (o1, o2) -> o1, HashMap::new));
    }

    @Override
    public void removeItemsWithColDotLessThan(int value) {
        map = map.entrySet()
                .stream()
                .filter(p -> p.getValue().getColDot() >= value)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (o1, o2) -> o1, HashMap::new));
    }

    @Override
    public int getSumColDot() {
        return map.values()
                .stream()
                .mapToInt(GeometricFigure::getColDot)
                .sum();
    }

    @Override
    public GeometricFigure get(int index) {
        return map.values()
                .stream()
                .skip(index - 1)
                .findFirst()
                .get();
    }

    @Override
    public List<GeometricFigure> getTwoItem(int index) {
        ArrayList<GeometricFigure> result =
                map.values()
                        .stream()
                        .skip(index - 1)
                        .limit(2)
                        .collect(Collectors.toCollection(ArrayList::new));
        return result;
    }

    @Override
    public List<GeometricFigure> getItems(GeometricFigure value) {
        ArrayList<GeometricFigure> result =
                map.values()
                        .stream()
                        .filter(geometricFigure -> geometricFigure.equals(value))
                        .collect(Collectors.toCollection(ArrayList::new));
        return result;
    }

    @Override
    public boolean isAllItemsNameNotNull() {
        return map.values().stream()
                .filter(geometricFigure -> null != geometricFigure.getName())
                .count() == map.values().stream()
                .count();
    }

    @Override
    public void addPostfix(String postfix) {
        map.values()
                .stream()
                .forEach(geometricFigure -> geometricFigure.setName(geometricFigure.getName().concat(postfix)));
    }

    @Override
    public void sortByName() {
        map = map.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.comparing(GeometricFigure::getName)))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

    @Override
    public void sortByColDot() {
        map = map.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(Comparator.comparing(GeometricFigure::getColDot)))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

    public void printElem() {
        map.values().stream().forEach(geometricFigure -> System.out.println(geometricFigure.toString()));
    }
}
